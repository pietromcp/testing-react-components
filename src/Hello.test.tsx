import { render, screen } from '@testing-library/react'
import Hello from './Hello'

test('renders Hello, World!', () => {
  render(<Hello />)
  const element = screen.getByText(/Hello, World!/i)
  expect(element).toBeInTheDocument()
})

test('renders Hello, Pietro!', () => {
  render(<Hello to="Pietro"/>)
  const element = screen.getByText(/Hello, Pietro!/i)
  expect(element).toBeInTheDocument()
})
