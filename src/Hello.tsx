interface HelloProps {
  to?: string
}

function Hello(props: HelloProps) {
  return (
    <span>Hello, {props.to || "World"}!</span>
  )
}

export default Hello
