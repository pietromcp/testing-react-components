import { render, screen } from '@testing-library/react'
import Counter from './Counter'

test('Renders default initial value', () => {
  render(<Counter />)
  const element = screen.getByText(/Current value is 0/i)
  expect(element).toBeInTheDocument()
})

test('Renders provided initial value', () => {
  render(<Counter value={19}/>)
  const element = screen.getByText(/Current value is 19/i)
  expect(element).toBeInTheDocument()
})

test('Handles value changes', () => {
  render(<Counter value={19}/>)
  const inc = screen.getByText(/Increment/i)
  inc.click()
  inc.click()
  inc.click()
  inc.click()
  const dec = screen.getByText(/Decrement/i)
  dec.click()
  const element = screen.getByText(/Current value is 22/i)
  expect(element).toBeInTheDocument()
})
