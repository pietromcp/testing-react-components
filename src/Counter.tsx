import { useState } from "react"

interface CounterProps {
  value?: number
}

function Counter(props: CounterProps) {
  const [value, setValue] = useState(props.value || 0)
  const increment = () => {
    setValue(value + 1)
  }

  const decrement = () => {
    setValue(value - 1)
  }
  return (
    <>
      <span>Current value is {value}</span>
      <button onClick={increment}>Increment</button>
      <button onClick={decrement}>Decrement</button>
    </>
  )
}

export default Counter
